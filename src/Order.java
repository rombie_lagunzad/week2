/*

Rombie Lagunzad
Fall 2020
Brother Lawrence

This program will help the food store to keep track of the current online orders
by displaying the person who order, the food included in the order and
the status of their order.

 */

import java.util.*;

public class Order {
    public static void main(String[] args) {
        System.out.println("Welcome to Jollibee's Order Inventory");
        Scanner input = new Scanner(System.in);

        // Use the set to avoid duplication of the order according to name
        Set people = new TreeSet();
        for (int i = 1; i < 4; i++) {
            System.out.println("Name of the person with order number " + i);
            String whoOrder = input.nextLine();
            // Data validation for the input to make sure that only string is included
            if (whoOrder.matches("[a-zA-Z ]+")) {
                people.add("Order number "+i+": "+whoOrder);
            }
            else {
                System.out.println("Invalid name! Please use letters.");
                --i;
            }
        }

        // Determine the correct total of order
        int size = people.size()+1;

        // Use queue for the next available delivery person
        Queue<String> deliveryMan = new PriorityQueue<>();
        deliveryMan.add("Luke");
        deliveryMan.add("Anakin");
        deliveryMan.add("Obi-Wan");
        deliveryMan.add("Han");

        // Use list with generics to create object
        List<CurrentOrder> foodOrder = new LinkedList<>();
        for (int i = 1; i < (size); i++) {
            System.out.println("What is included in order number " + i + "?");
            String personOrder = input.nextLine();

            // Data validation for the input to make sure that only string is included
            if (personOrder.matches("[a-zA-Z ]+")) {
                foodOrder.add(new CurrentOrder(i, personOrder, deliveryMan.peek()));
                // Remove the next delivery guy that was assigned for delivery
                deliveryMan.poll();
            }
            else {
                System.out.println("Invalid order! Please use letters");
                --i;
            }
        }

        // Map interface using TreeMap to set the keys and values in order by the key
        Map <Integer, String> status = new TreeMap<>();
        status.put(1,"The order is being prepared");
        status.put(2,"Ready for delivery");
        status.put(3,"Successfully delivered");

        // Create a list using arraylist to store the status of the order
        List update = new ArrayList();
        for (int i = 1; i < (size); i++) {
            System.out.println("\nWhat is the status of order number " + i + "?" + " (prepare, ready, delivered)");
            System.out.println("Type prepare if the order is being prepared");
            System.out.println("Type ready if the order is ready for delivery");
            System.out.println("Type delivered if the order is successfully delivered");
            String statusOrder = input.nextLine();

            // Validate the input and add the order status in the list named update
            if (statusOrder.matches("[a-zA-Z ]+")) {
                if (statusOrder.equals("prepare")) {
                    String orderUpdate = status.get(1);
                    update.add ("A: High Priority (order number "+i+") "+orderUpdate);
                }
                else if (statusOrder.equals("ready")){
                    String orderUpdate = status.get(2);
                    update.add ("B: Medium Priority (order number "+i+") "+orderUpdate);
                }
                else if (statusOrder.equals("delivered")){
                    String orderUpdate = status.get(3);
                    update.add ("C: Low Priority (order number "+i+") "+orderUpdate);
                }
                else {
                    System.out.println("Invalid input! Please type the required keyword");
                    --i;
                }
            }
            else {
                System.out.println("Invalid input! Please use letters.");
                --i;
            }
        }

        // Comparator to sort according to urgency level
        Comparator<String> urgency = new OrderUrgency();
        Collections.sort(update,urgency);

        // Display the summary of order
        System.out.println("\n**LIST OF PEOPLE WITH ORDER**");
        for ( Object who : people) {
            System.out.println((String)who);
        }
        System.out.println("\n**LIST OF ORDERS AND WHO WILL DELIVER**");
        for (CurrentOrder order : foodOrder) {
            System.out.println(order);
        }
        System.out.println("\n**ORDER URGENCY LEVEL AND STATUS**");
        for ( Object where : update) {
            System.out.println((String)where);
        }
    }
}

