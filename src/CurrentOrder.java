public class CurrentOrder {

    private int orderNumber;
    private String food;
    private int quantity;
    private String deliveryMan;

    public CurrentOrder(int orderNumber, String food, String deliveryMan) {
        this.orderNumber = orderNumber;
        this.food = food;
        this.deliveryMan = deliveryMan;
    }

    public String toString() {
        return "\nOrder Number " + orderNumber + "\n" + food + "\nAssigned Delivery Man: " + deliveryMan;
    }
}
